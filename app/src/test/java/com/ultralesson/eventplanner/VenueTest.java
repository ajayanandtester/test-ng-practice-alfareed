package com.ultralesson.eventplanner;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ultralesson.eventplanner.model.Venue;

public class VenueTest {
    @Test
    public void TestVenueCreation(){
        Venue venue = new Venue(1, "Grand Ballroom", "123 Main St", 500);
        Assert.assertEquals(venue.getId(),1);
        Assert.assertEquals(venue.getName(), "Grand Ballroom");
        Assert.assertEquals(venue.getAddress(),"123 Main St");
        Assert.assertEquals(venue.getCapacity(), 500);
    }
}
