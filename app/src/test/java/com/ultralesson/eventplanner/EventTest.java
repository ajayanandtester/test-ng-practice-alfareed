package com.ultralesson.eventplanner;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ultralesson.eventplanner.model.Event;
import com.ultralesson.eventplanner.model.Venue;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class EventTest {
    @BeforeClass
    public void setUpClass() {
        System.out.println("Setting up resources for EventTest class...");
    }

    @AfterClass
    public void tearDownClass() {
        System.out.println("Releasing resources for EventTest class...");
    }

    @Test
    public void testEventCreation() {
        Venue venue = new Venue(1, "Grand Ballroom", "123 Main St", 500);
        Event event = new Event(1, "Wedding", "A beautiful wedding ceremony.", venue);

        Assert.assertEquals(event.getId(), 1);
        Assert.assertEquals(event.getName(), "Wedding");
        Assert.assertEquals(event.getDescription(), "A beautiful wedding ceremony.");
        Assert.assertEquals(event.getVenue(), venue);
    }
    
}
